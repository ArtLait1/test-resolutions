import { useState } from 'react';
import '../App.scss';

export default function WithoutPictureWithWebp() {
  const [showWebp, setShowWebp] = useState(null);
  setTimeout(() => {
    console.log(window.Modernizr);
    if (window.Modernizr.webp.valueOf()) {
      setShowWebp(true);
      console.log('supported');
    } else {
      setShowWebp(false);
      console.log('not-supported');
    }
  }, 0);
  console.log('render');
  return (
      <div className="container">
        <div className="left">
            <div className="left__main">
              { showWebp === true
                && <img
                    className="webp"
                    srcSet="https://via.placeholder.com/428.webp?text=428(webp) 2x,
                            https://via.placeholder.com/512.webp?text=512(webp) 1x"
                    src="https://via.placeholder.com/512" alt="test selected item without webp" />}
              { showWebp === false
                && <img
                    className="webp"
                    srcSet="https://via.placeholder.com/428.jpg?text=428(jpg) 2x,
                            https://via.placeholder.com/512.jpg?text=512(jpg) 1x"
                    src="https://via.placeholder.com/512" alt="test selected item without webp" />
              }
            </div>
            <div className="left__feed feed">
                {[1, 2, 4, 5].map((item, index) =>
                  <div key={item} className="feed__item">
                    { showWebp === true
                      && <img
                          className="webp feed__img"
                          srcSet="https://via.placeholder.com/112.webp?text=112(webp) 2x,
                                  https://via.placeholder.com/80.webp?text=80(webp) 1x"
                          src="https://via.placeholder.com/512"
                          alt="test item" />}
                    { showWebp === false 
                      && <img
                          className="webp feed__img"
                          srcSet="https://via.placeholder.com/112.jpg?text=112(jpg) 2x,
                                  https://via.placeholder.com/80.jpg?text=80(jpg) 1x"
                          src="https://via.placeholder.com/512"
                          alt="test item" />}
                  </div>)}
            </div>
        </div>
        <div className="right">
          Without picture with webp
        </div>
    </div>
  );
}
  