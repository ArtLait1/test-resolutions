import '../App.scss';

export default function HasPicture() {
  return (
      <div className="container">
        <div className="left">
            <div className="left__main">
              <picture>
                <source srcset="https://via.placeholder.com/214.webp/?text=214(webp) 1x, https://via.placeholder.com/428.webp/?text=428(webp) 2x" media="(max-width: 1000px)" type="image/webp" />
                <source srcset="https://via.placeholder.com/512.webp/?text=512(webp) 1x, https://via.placeholder.com/1024.webp/?text=1024(webp) 2x" media="(max-width: 3000px)" type="image/webp" />
                <source srcset="https://via.placeholder.com/214.jpeg/?text=214(jpeg) 1x, https://via.placeholder.com/428.jpeg/?text=428(jpeg) 2x" media="(max-width: 1000px)"  type="image/jpeg" />
                <source srcset="https://via.placeholder.com/512.jpeg/?text=512(jpeg) 1x, https://via.placeholder.com/1024.jpeg/?text=1024(jpeg) 2x" media="(max-width: 3000px)"  type="image/jpeg" />
                <img src="https://via.placeholder.com/512/?text=default" alt="selected item" />  
              </picture>
            </div>
            <div className="left__feed feed">
                {[1, 2, 4, 5].map((item, index) =>
                  <div key={item} className="feed__item">
                    <picture>
                      <source srcset="https://via.placeholder.com/56.webp/?text=56(webp) 1x, https://via.placeholder.com/112.webp/?text=112(webp) 2x" media="(max-width: 1000px)" type="image/webp" />
                      <source srcset="https://via.placeholder.com/80.webp/?text=80(webp) 1x, https://via.placeholder.com/160.webp/?text=160(webp) 2x" media="(max-width: 3000px)" type="image/webp" />
                      <source srcset="https://via.placeholder.com/56.jpeg?text=56(jpeg) 1x, https://via.placeholder.com/112.jpeg?text=112(jpeg) 2x" media="(max-width: 1000px)"  type="image/jpeg" />
                      <source srcset="https://via.placeholder.com/80.jpeg?text=80(jpeg) 1x, https://via.placeholder.com/160.jpeg?text=160(jpeg) 2x" media="(max-width: 3000px)"  type="image/jpeg" />
                      <img src="https://via.placeholder.com/512/?text=default" alt="item al list" />
                    </picture>
                  </div>)}
            </div>
        </div>
        <div className="right">
          Has picture
        </div>
    </div>
  );
}
