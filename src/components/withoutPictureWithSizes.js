import '../App.scss';

export default function WithoutPictureWithSizes() {
    return (
        <div className="container">
          <div className="left">
              <div className="left__main">
               <img
                className="webp img-main"
                srcSet="https://via.placeholder.com/428.jpg?text=428(jpg) 428w,
                        https://via.placeholder.com/512.jpg?text=512(jpg) 512w"
                sizes="(max-width: 500px) 141px,
                        (max-width: 3000px) 512px,
                        800px"
                src="https://via.placeholder.com/512" alt="test selected item without webp" />
              </div>
              <div className="left__feed feed">
                  {[1, 2, 4, 5].map((item, index) =>
                    <div key={item} className="feed__item">
                      <img
                        className="webp feed__img"
                        srcSet="https://via.placeholder.com/80.jpg?text=80(jpg) 80w,
                                https://via.placeholder.com/80.jpg?text=80(jpg) 80w"
                        sizes="(max-width: 500px) 80px,
                                (max-width: 3000px) 80px,
                                800px"
                        src="https://via.placeholder.com/512"
                        alt="test item" />
                    </div>)}
              </div>
          </div>
          <div className="right">
            Without picture with sizes
          </div>
      </div>
    );
  }