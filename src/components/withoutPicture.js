import '../App.scss';

export default function WithoutPicture() {
    
    return (
        <div className="container">
          <div className="left">
              <div className="left__main">
               <img
                className="webp"
                srcSet="https://via.placeholder.com/428.jpg?text=428(jpg) 2x,
                        https://via.placeholder.com/512.jpg?text=512(jpg) 1x"
                src="https://via.placeholder.com/512" alt="test selected item without webp" />
              </div>
              <div className="left__feed feed">
                  {[1, 2, 4, 5].map((item, index) =>
                    <div key={item} className="feed__item">
                      <img
                        className="webp feed__img"
                        srcSet="https://via.placeholder.com/112.jpg?text=11x(jpg) 2x,
                                https://via.placeholder.com/80.jpg?text=80(jpg) 1x"
                        src="https://via.placeholder.com/512"
                        alt="test item" />
                    </div>)}
              </div>
          </div>
          <div className="right">
            Without picture
          </div>
        
      </div>
    );
  }