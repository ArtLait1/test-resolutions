import './App.scss';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import HasPicture from './components/hasPicture';
import WithoutPicture from './components/withoutPicture';
import WithoutPictureWithWebp from './components/withoutPictureWithWebp';
import WithoutPictureWithSizes from './components/withoutPictureWithSizes';

function App() {
  return (
    <Router>
      <nav className="navigation-container">
          <ul className="navigation">
            <li className="navigation__item">
              <Link to="/has-picture">has picture</Link>
            </li>
            <li className="navigation__item">
              <Link to="/without-picture">without picture</Link>
            </li>
            <li className="navigation__item">
              <Link to="/without-picture-with-webp">without picture with webp</Link>
            </li>
            <li className="navigation__item">
              <Link to="/without-picture-with-sizes">without picture with sizes</Link>
            </li>
          </ul>
        </nav>
      <Switch>
        <Route path="/has-picture">
          <HasPicture />
        </Route>
        <Route path="/without-picture">
          <WithoutPicture />
        </Route>
        <Route path="/without-picture-with-webp">
          <WithoutPictureWithWebp />
        </Route>
        <Route path="/without-picture-with-sizes">
          <WithoutPictureWithSizes />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
